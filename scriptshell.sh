#!/bin/bash

# Fonction pour afficher l'aide
afficher_aide() {
    echo "Utilisation : sh scripthsell.sh 'data.csv' et deuxième arguments(voir options)"
    echo "Options :"
    echo "  -h      Afficher l'aide"
    echo "Traitements disponibles :"
    echo "  -d1   Les 10 Conducteurs avec le plus de trajets"
    echo "  -d2   Les 10 Conducteurs ayant la plus grande distance"
    echo "  -l    Les 10 trajets les plus longs"
    echo "  -s    Les 10 villes les plus traversées"
    echo "  -t    Statistiques sur les étapes"
}

# Afficher l'aide
if [ "$1" = "-h" -o "$2" = "-h" ]; then
    afficher_aide
    exit 0
fi

# Vérifier le nombre d'arguments
if [ "$#" -lt 2 ]; then
    echo "Erreur : Au moins deux arguments sont requis."
    afficher_aide
    exit 1
fi

# Vérifier si le fichier CSV existe
if [ ! -f "$1" ]; then
    echo "Erreur : Le fichier CSV 'data.csv' n'existe pas."
    exit 2
fi

# Vérifier et créer le dossier temp
dossier_temp="temp"
if [ ! -d "$dossier_temp" ]; then
    mkdir "$dossier_temp"
    echo "Création dossier 'temp'."
else    
    rm -r "$dossier_temp"
    mkdir "$dossier_temp"
fi

# Vérifier et créer le dossier images
dossier_images="images"
if [ ! -d "$dossier_images" ]; then
    mkdir "$dossier_images"
    echo "Création dossier 'images'."
fi

# Vérifier et créer le dossier demo
dossier_demo="demo"
if [ ! -d "$dossier_demo" ]; then
    mkdir "$dossier_demo"
    echo "Création dossier 'demo'."
fi

# Créer les exécutables
cd progc/
make
cd ..


# Switch/case pour les traitements
for traitement in "$2"; do
    case "$traitement" in
        -d1)
            # Traitement d1 - Conducteurs avec le plus de trajets
            # Vérifier la présence de l'exécutable
            if [ ! -x 'progc/exeD1' ] ; then
            	echo "Erreur : L'exécutable 'exeD1' n'existe pas."
            	exit 10
            fi
            time cat "$1" | tail +2 | cut -d';' -f1,6 | sort -u| progc/exeD1 |sort -t';' -k2 -n -r | head -10 > datad1.dat
            gnuplot "progc/histogramme_option_d1.gnu"
            convert histogramme_option_d1.png -rotate 90 histogramme_option_d1.png
            cp datad1.dat demo/
            cp progc/histogramme_option_d1.gnu demo/
            cp histogramme_option_d1.png demo/
            mv datad1.dat temp/
            mv histogramme_option_d1.png images/
            echo "Traitement d1 réalisé avec succès et déposé dans le fichier 'images'."
            ;;
        -d2)
            # Traitement d2 - Conducteurs et la plus grande distance
            # Vérifier la présence de l'exécutable
            if [ ! -x 'progc/exeD2' ] ; then
            	echo "Erreur : L'exécutable 'exeD2' n'existe pas."
            	exit 11
            fi
            time cat "$1" | tail +2 | cut -d';' -f5,6 | progc/exeD2 | sort -t';' -k2 -n -r | head -10 > datad2.dat
            gnuplot "progc/histogramme_option_d2.gnu"
            convert histogramme_option_d2.png -rotate 90 histogramme_option_d2.png
            cp datad2.dat demo/
            cp progc/histogramme_option_d2.gnu demo/ 
            cp histogramme_option_d2.png demo/          
            mv datad2.dat temp/
            mv histogramme_option_d2.png images/
            echo "Traitement d2 réalisé avec succès et déposé dans le fichier 'images'." 
            ;;
        -s)
            # Traitement s - Les 10 villes les plus traversées
            # Vérifier la présence de l'exécutable
            if [ ! -x 'progc/exeS' ] ; then
            	echo "Erreur : L'exécutable 'exeS' n'existe pas."
            	exit 12
            fi
            time cat "$1" | tail +2 | cut -d';' -f1,5 | progc/exeS | head -50  > datas.dat
            gnuplot "progc/histogramme_option_s.gnu"
            cp datas.dat demo/
            cp progc/histogramme_option_s.gnu demo/
            cp graphique_option_s.png demo/
            mv datas.dat temp/
            mv graphique_option_s.png images/
            echo "Traitement S réalisé avec succès et déposé dans le fichier 'images'."
            ;;
        -l)
            # Traitement l - Les 10 trajets les plus longs
            # Vérifier la présence de l'exécutable
            if [ ! -x 'progc/exeL' ] ; then
            	echo "Erreur : L'exécutable 'exeL' n'existe pas."
            	exit 13
            fi
            time cat "$1" | tail +2 | cut -d';' -f1,5 | progc/exeL | sort -t',' -k2 -n -r | head -10 | sort -n -r > datal.dat
            gnuplot "progc/histogramme_option_l.gnu"
            cp datal.dat demo/
            cp progc/histogramme_option_l.gnu demo/
            cp histogramme_option_l.png demo/
            mv datal.dat temp/
            mv histogramme_option_l.png images/
            echo "Traitement L réalisé avec succès et déposé dans le fichier 'images'."
            ;;
        -t)
            # Traitement t - Statistiques sur les étapes
            # Vérifier la présence de l'exécutable
            if [ ! -x 'progc/exeT' ] ; then
            	echo "Erreur : L'exécutable 'exeT' n'existe pas."
            	exit 14
            fi
            time cat "$1" | tail +2 | cut -d';' -f1,2,3,4| progc/exeT | head -10 > datat.dat
            gnuplot "progc/histogramme_option_t.gnu"
            cp datat.dat demo/
            cp progc/histogramme_option_t.gnu demo/
            cp histogramme_option_t.png demo/
            mv datat.dat temp/
            mv histogramme_option_t.png images/
            echo "Traitement T réalisé avec succès et déposé dans le fichier 'images'." 
            ;;
        *)
            echo "Erreur : Traitement '$traitement' non reconnu."
            echo "Voir l'aide avec -h"
            exit 1
            ;;
    esac
done
