set terminal png size 640, 680 #définit la taille de l'image
set output "histogramme_option_d2.png" #sauvegarde le graphique dans le fichier "histogramme_option_d2.png"
set style data histogram #définit le graphique en mode histogramme
set datafile sep ";" #définit le séparateur 
set style fill solid border -1 #fait en sorte qu'il y ait une bordure pour chaque barre
set style fill transparent solid 0.5 noborder #fait en sorte les barres soient transparents
set ytics 20000 #définit la graduation de l'axe des ordonnées
set ytics rotate by 90 scale 1 #tourne de 90° les ordonnées 
set yrange [0:160000] #définit la plage de valeurs de l'axe des ordonnées
set xlabel ' ' #définit le titre de l'axe des abscisses
set xtics offset 2, graph -0.02 #décale/déplace/change de position les noms
set xtic rotate by 90 scale 1 #tourne de 90° les abscisses 
set label 1 'CHAUFFEURS' at graph 0.5, -0.06 centre rotate by 180 #fait apparaître le texte "CHAUFFEURS" selon les coordonnée
set label 2 'Option d2' at graph -0.05, 0.45 left rotate by 90 #fait apparaître le texte "Option d2" selon les coordonnées
set label 3 'DISTANCE (Km)' at graph 1.03, 0.45 left rotate by 90 #fait apparaître le texte "NB ROUTES" selon les coordonnées
plot 'datad2.dat' using 2:xtic(1) notitle #trace des barres avec pour ordonnée les données de la colonne 2 du 'datad2.dat' et abscisse les données de la colonne 1 
