#ifndef OPTION_L
#define OPTION_L

//PréIng2 MI2 Semestre 1, Projet CY-Trucks : Traitement -l
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    float distance;
    int routeid;
    char nom[100];
    char villeA[100];
    char villeB[100];
} Chauffeur;

typedef struct _tree {
    int value;
    Chauffeur *chauffeur;
    struct _tree *pL;
    struct _tree *pR;
} Tree;

int hash(char *c);
Tree *createTree(int v);
Tree *addTree(Tree *p, int v, float dist);
void displayPrefixe(Tree* p);
void freeTree(Tree *p);
//Rentrer time cat data.csv | tail +2 | cut -d';' -f1,5 | ./etu | sort -t',' -k2 -n -r | head -10 | sort -n -r
#endif