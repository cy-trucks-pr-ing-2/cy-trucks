#include "optionL.h"

int hash(char *c) {
    int h = 0, i = 0;
    if (c == NULL) {
        exit(1);
    }
    while (c[i] != '\0') {
        h = 31 * h + c[i];
        i++;
    }
    return h;
}

Tree *createTree(int v) {
    Tree *pNew = malloc(sizeof(Tree));
    if (pNew == NULL) {
        exit(1);
    }
    pNew->value = v;
    pNew->pL = NULL;
    pNew->pR = NULL;
    pNew->chauffeur = NULL;
    return pNew;
}

Tree *addTree(Tree *p, int v, float dist) {
    if (p == NULL) {
        p = createTree(v);
        if (p == NULL) {
            exit(2);
        }
        p->chauffeur = malloc(sizeof(Chauffeur));
        p->chauffeur->routeid = v;
        p->chauffeur->distance = dist;
    } else {
        if (v < p->value) {
            p->pL = addTree(p->pL, v, dist);
        } else if (v > p->value) {
            p->pR = addTree(p->pR, v, dist);
        } else { // v = p->value
            p->chauffeur->distance += dist; //on somme les distances
        }
    }
    return p;
}
void displayPrefixe(Tree* p) {
    if (p != NULL) {
       printf( "%d,%f\n", p->chauffeur->routeid, p->chauffeur->distance);
        displayPrefixe(p->pL);
        displayPrefixe(p->pR);
    }
}


void freeTree(Tree *p) {
    if (p != NULL) {
        freeTree(p->pL); // Free sous arbre gauche
        freeTree(p->pR); // Free sous arbre droit
        free(p->chauffeur); // Free Chauffeur
        free(p);
    }
}

int main() {
    Tree *Arbre = NULL;
    int routeid;
    float dist;

    // Lecture des noms et distances pour remplir l'arbre
    while (scanf("%d;%f", &routeid, &dist) == 2) {
        Arbre = addTree(Arbre, routeid, dist);
    }

    displayPrefixe(Arbre);
    freeTree(Arbre);
    return 0;
}
//Rentrer time cat data.csv | tail +2 | cut -d';' -f1,5 | ./etu | sort -t',' -k2 -n -r | head -10 | sort -n -r