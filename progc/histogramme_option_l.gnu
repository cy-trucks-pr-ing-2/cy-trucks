set terminal png size 640, 600 #définit la taille de l'image
set title "histogramme option l" #donne un titre au graphique
set output "histogramme_option_l.png" #sauvegarde le graphique dans un fichier histogramme_l.png
set xlabel "ROUTE ID" #donne un titre à l'axe des abscisses
set ylabel "Distance (km)" #donne un titre à l'axe des ordonnées
set style data histograms #définit le style de graphique
set boxwidth 0.9 absolute #définit la largeur des bâtons 
set ytics 500 #définit la graduation en ordonnées
set yrange [0:3000] #définit la plage de valeurs en ordonnées
set style histogram cluster gap 1 #fait en sorte que l'histogramme soit un histogramme regroupé
set style fill solid border -1 #fait en sorte que les bâtons soient remplis
set style fill transparent solid 0.5 noborder #fait en sorte les barres soient transparents
set datafile sep "," #définit le séparateur
set xtic rotate by -45 scale 0 #fait pivoter de 45° les abscisses
plot "datal.dat" using 1:xtic(1) notitle lc rgb "white",\
     "datal.dat" using 2 with boxes notitle
