#include "optiond1.h"

int hash(char *c) {
    int h = 0, i = 0;
    if (c == NULL) {
        exit(1);
    }
    while (c[i] != '\0') {
        h = 31 * h + c[i];
        i++;
    }
    return h;
}

Tree *createTree(int v, int num, char *nom) {
    Tree *pNew = malloc(sizeof(Tree));
    if (pNew == NULL) {
        exit(1);
    }
    pNew->value = v;
    pNew->pL = NULL;
    pNew->pR = NULL;
    pNew->chauffeur = malloc(sizeof(Chauffeur));
    if (pNew->chauffeur == NULL) {
        exit(1);
    }
    strcpy(pNew->chauffeur->nom, nom);
    pNew->chauffeur->routeid = num;
    pNew->chauffeur->numtrajet = 1; // Initialiser à 1
    return pNew;
}

Tree *addTree(Tree *p, int v, int num, char *nom) {
    if (p == NULL) {
        p = createTree(v, num, nom);
	    if (p == NULL) {
            exit(2);
        }
    } 
    else {
        if (v < p->value) {
            p->pL = addTree(p->pL, v, num, nom);
        } else if (v > p->value) {
            p->pR = addTree(p->pR, v, num, nom);
        } else {
	     // Chauffeur existant, vérifier le numéro de route
            if (p->chauffeur->routeid != num) {
                p->chauffeur->numtrajet =  p->chauffeur->numtrajet + 1;
                //p->chauffeur->routeid = num;
        }
    }
}
    return p;
}

void displayPrefixe(Tree *p) {
    if (p != NULL) {
        printf("%s;%d\n", p->chauffeur->nom, p->chauffeur->numtrajet);
        displayPrefixe(p->pL);
        displayPrefixe(p->pR);
    }
}

void freeTree(Tree *p) {
    if (p != NULL) {
        freeTree(p->pL); // Free sous arbre gauche
        freeTree(p->pR); // Free sous arbre droit
        free(p->chauffeur); // Free Chauffeur
        free(p);
    }
}

int main() {
    Tree *Arbre = NULL;
    int num;
    char nom[100];
    // Lecture des noms et numéros de trajet pour remplir l'arbre
    while (scanf("%d;%[^\n]\n", &num, nom) == 2) { //et pas %s afin de lire tout jusqu'a changer de ligne d'où le ^\n
        Arbre = addTree(Arbre, hash(nom), num, nom);
    }

    // Afficher l'arbre dans l'ordre décroissant des distances avec les noms des chauffeurs
    displayPrefixe(Arbre);

    // Libérer la mémoire allouée pour l'arbre
    freeTree(Arbre);

    return 0;
}
//gcc -o ert optiond1.c
//A entrer dans Unix : time cat data.csv | tail +2 | cut -d';' -f1,6 | sort -u | ./ert | sort -t';' -k2 -n -r | head -10
