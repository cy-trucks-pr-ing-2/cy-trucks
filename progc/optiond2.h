#ifndef OPTION_D2
#define OPTION_D2

//PréIng2 MI2 Semestre 1, Projet CY-Trucks : Traitement -d2
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    float distance;
    char nom[100];
    char villeA[100];
    char villeB[100];
} Chauffeur;

typedef struct _tree {
    int value;
    Chauffeur *chauffeur;
    struct _tree *pL;
    struct _tree *pR;
} Tree;

int hash(char *c);
Tree *createTree(int v);
Tree *addTree(Tree *p, int v, float dist, char *nom);
void displayPrefixe(Tree* p);
void freeTree(Tree *p);
//POUR UNIX RENTRER CA : gcc -o exouma optiond2.c
//time cat data.csv | tail +2 | cut -d';' -f5,6 | ./exouma | sort -t';' -k2 -n -r | head -10

#endif