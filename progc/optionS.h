#ifndef OPTION_S
#define OPTION_S

//PréIng2 MI2 Semestre 1, Projet CY-Trucks : Traitement -s
#include <stdio.h>
#include <stdlib.h>

typedef struct _DistanceNode {
    float distance;
    struct _DistanceNode* pNext;
} Distance;    //stocker plusieurs distances ayant le même numéro de routes

typedef struct _AVL{
    int routeid;
    Distance* distance;
    struct _AVL* pL;
    struct _AVL* pR;
    int taille;
    float minDistance;
    float maxDistance;
    float moyenne;
    float moyennedistance;
    int nombredist;
    float sommedist;
} AVL;

Distance* Newdistance(float dist);
AVL* createAVL(int routeNbr, float dist);

//utile pour le remplissage du 2ème AVL
AVL* createAVL2(int routeNbr, float maxdist, float mindist, float moyenne, float moyennedist);
int taille(AVL* p);
int getEquilibre(AVL* p);

AVL* rotationDroite(AVL* p);
AVL* rotationGauche(AVL* p);
AVL* insertAVL(AVL* p, int routenbr, float dist);

// Fonction utilitaire pour insérer un nœud dans l'AVL p2 trié par distance
AVL* insertDistance(AVL* p2, int numid, float maxdist, float mindist, float moyenne, float moyennedistance);

//fonction pour parcourir notre premier AVL et mettres ses données dans le 2ème AVL
AVL* insertAVL2(AVL* p, AVL* p2);
void displayInfixe(AVL* p);
void destroyAVL(AVL* p);

//A RENTRER DANS UNIX : gcc -o traitements optionS.c
// time cat data.csv | tail +2 | cut -d';' -f1,5 | ./traitements | head -50

#endif