reset
set terminal png size 1000, 800 #défnit la taille de l'image
set title "Option t" #donne un titre au graphique 
set output "histogramme_option_t.png" #sauvegarde le graphique dans le fichier histogramme_option_t.png
set style data histograms #fait en sorte que le graphique soit de type histogramme
set style histogram cluster gap 1 #fait en sorte que l'histogramme soit un histogramme regroupé
set boxwidth 0.9 absolute #définit la largeur des barres
set xlabel 'NOM DES VILLES' #donne un titre à l'axe des abscisses
set ylabel 'NB ROUTES' #donne un titre à l'axe des ordonnées
set yrange [0:4000] #donne une plage de valeurs à l'axe des ordonnées
set ytics 500 #donne une graduation à l'axe des ordonnées
set style fill solid border -1 #donne une bordure aux barres
set style fill transparent solid 0.5 noborder #fait en sorte que les barres soient transparentes
set xtic rotate by -45  scale 1 #tourne de 45° les abscisses 
set datafile sep ';' #définit le séparateur
set key top right box #met des bordures autour des légendes
set key width 1 #définit la taille des bordures
plot 'datat.dat' using :xtic(1) lc rgb "white" notitle,\
     'datat.dat' using 2:xtic(1) title 'Routes Totales',\
     'datat.dat' using 3:xtic(1) title 'Premières villes'

