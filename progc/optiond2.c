#include "optiond2.h"

int hash(char *c) {
    int h = 0, i = 0;
    if (c == NULL) {
        exit(1);
    }
    while (c[i] != '\0') {
        h = 31 * h + c[i];
        i++;
    }
    return h;
}

Tree *createTree(int v) {
    Tree *pNew = malloc(sizeof(Tree));
    if (pNew == NULL) {
        exit(1);
    }
    pNew->value = v;
    pNew->pL = NULL;
    pNew->pR = NULL;
    pNew->chauffeur = NULL;
    return pNew;
}

Tree *addTree(Tree *p, int v, float dist, char *nom) {
    if (p == NULL) {
        p = createTree(v);
        if (p == NULL) {
            exit(2);
        }
        p->chauffeur = malloc(sizeof(Chauffeur));
        strcpy(p->chauffeur->nom, nom); //copier le contenu d'une chaîne de charactère
        p->chauffeur->distance = dist;
    } else {
        if (v < p->value) {
            p->pL = addTree(p->pL, v, dist,nom);
        } else if (v > p->value) {
            p->pR = addTree(p->pR, v, dist,nom);
        } else {
            p->chauffeur->distance += dist;
        }
    }
    return p;
}

void displayPrefixe(Tree* p) {
    if (p != NULL) {
       printf( "%s;%f\n", p->chauffeur->nom, p->chauffeur->distance);
        displayPrefixe(p->pL);
        displayPrefixe(p->pR);
    }
}

void freeTree(Tree *p) {
    if (p != NULL) {
        freeTree(p->pL); // Free sous arbre gauche
        freeTree(p->pR); // Free sous arbre droit
        free(p->chauffeur); // Free Chauffeur
        free(p);
    }
}

                                             

int main() {
    Tree *Arbre = NULL;
    float dist;
    char nom[100];
    // Lecture des noms et distances pour remplir l'arbre
    while (scanf("%f;%[^\n]\n", &dist, nom) == 2) { //et pas %s afin de lire tout jusqu'a changer de ligne d'où le \n
        Arbre = addTree(Arbre, hash(nom), dist, nom);
    }

    // Afficher l'arbre dans l'ordre décroissant des distances avec les noms des chauffeurs
 displayPrefixe(Arbre);
freeTree(Arbre);
    return 0;
}

// POUR UNIX RENTRER CA : gcc -o exouma optiond2.c
//time cat data.csv | tail +2 | cut -d';' -f5,6 | ./exouma | sort -t';' -k2 -n -r | head -10