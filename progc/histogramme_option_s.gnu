set terminal png size 1500,800 #définit la taille de l'image
set title "Option S" #donne un titre au graphique
set output "graphique_option_s.png" #sauvegarde le graphique dans le fichier graphique_option_s.png
set xlabel "ROUTE ID" #donne un titre à l'axe des abscisses
set ylabel "DISTANCE (Km)" #donne un titre à l'axe des ordonnées
set style data lines #définit le graphique en mode lignes
set style line 1 lc rgb "blue" linewidth 2 #définit la largeur de la courbe
set ytics 100 #définit la graduation de l'axe des ordonnées
set yrange [0:1000] #définit la plage des valeurs de l'axe des ordonnées
set style fill transparent solid 0.5 noborder #fait en osrte que les barres soient transparentes
set datafile sep ";" #définit le spérateur
set xtic rotate by -55 scale 1 #tourne de 50° les abscisses
set key top right box #met des bordures autour des légendes
set key width 1 #définit la taille des bordures
plot "datas.dat" using :xtic(1) notitle,\
     "datas.dat" using 4:xtic(1) with filledcurve above title "Distances max/min (Km)",\
     "datas.dat" using 3:xtic(1) title "Distance moyenne" linestyle 1,\
     "datas.dat" using 2:xtic(1) with filledcurve above notitle lc rgb "white"
