#ifndef OPTION_D1
#define OPTION_D1

//PréIng2 MI2 Semestre 1, Projet CY-Trucks : Traitement -d1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int numtrajet;
    int routeid;
    char nom[100];
    char villeA[100];
    char villeB[100];
} Chauffeur;

typedef struct _tree {
    int value;
    Chauffeur *chauffeur;
    struct _tree *pL;
    struct _tree *pR;
} Tree;

int hash(char *c);
Tree *createTree(int v, int num, char *nom);
Tree *addTree(Tree *p, int v, int num, char *nom);
void displayPrefixe(Tree *p);
void freeTree(Tree *p);
//gcc -o ert optiond1.c
//A entrer dans Unix : time cat data.csv | tail +2 | cut -d';' -f1,6 | sort -u | ./ert | sort -t';' -k2 -n -r | head -10

#endif