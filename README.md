# Projet-Informatique: CY TRUCKS


## Description brève
Notre projet a pour but de traiter les données d'une société importante de transport routier. Les données de cette entreprise sont stockées dans un fichier nommé data.csv (6 millions de lignes et + de 300Mo). Notre programme permet donc d'analyser le contenu de ce fichier et de générer des graphiques résumant son contenu.


## Installation
Pour installer l'ensemble desdossiers et fichiers du projet, il vous suffit d'ouvrir votre terminal et de taper la commande : git clone https://gitlab.com/cy-trucks-pr-ing-2/cy-trucks .


## Compilation et execution
Pour compiler et exécuter le programme, il vous suffit d'ouvrir votre terminal :

![Ouverture du terminal](Images%20README/Ouverture_terminal.PNG)

1. Placez vous à la racine du projet à l'aide de la commande cd :

![Racine du projet](Images%20README/Racine_projet_cd.PNG)

2. Pour savoir quel traitement appliqué au fichier data.csv, il vous suffit d'écrire dans le terminal : " bash scriptshell.sh data/data.csv -h ". L'option **-h** utilisée permet d'afficher les traitements disponibles à effectuer sur le fichier data.csv.

![Commande d'exécution](Images%20README/Détails_avant_exécution.PNG)

3. Vous pouvez ensuite appliquer l'un des 5 traitements disponibles en écrivant : " bash scriptshell.sh data/data.csv ***-option_choisie*** ". Veillez ainsi à remplacer " -option_choisie " par : **-d1**, **-d2**, **-l**, **-s** ou **-t**.

![Exemple de traitement choisi](Images%20README/Commande_exécution_exemple.PNG)

4. Il n'y a plus qu'à appuyer sur la touche ENTREE de votre ordinateur pour lancer le programme avec l'option que vous avez choisi !


5. Des exécutables et des fichiers avec l'extension ".o" sont créés dans le dossier progc. Pour les supprimer :
    1. écrivez dans votre terminal à partir de la racine du projet : "cd progc/"
    2. tapez dans le terminal la commande : "make clean"

A noter : la commande "make" est utilisée dans le fichier "scriptshell.sh" pour compiler le projet. Si toutefois vous souhaitez le compiler par vous-même, tapez dans votre terminal (depuis la racine du projet) : "cd progc/" puis "make". Pour appliquer ensuite le traitement de votre choix, il vous suffit de revenir à la racine du projet en tapant "cd .." et vous pouvez effectuer le traitement voulu comme expliquer ci-dessus à partir du 1.


## Visualisation des graphiques
Les graphiques du ou des traitements effectués sont disponibles dans le dossier "images", accessible depuis la racine du projet. Il suffit de double-cliquer sur l'une des images pour l'ouvrir.

Voici les images que vous devez obtenir obtenir selon le ou les traitements effectués :
- pour le **traitement -d1** :
![histogramme_option_d1.png](Images%20README/histogramme_option_d1.png)

- pour le **traitement -d2** :
![histogramme_option_d2.png](Images%20README/histogramme_option_d2.png)

- pour le **traitement -l** :
![histogramme_option_l.png](Images%20README/histogramme_option_l.png)

- pour le **traitement -s** :
![graphique_option_s.png](Images%20README/graphique_option_s.png)

- pour le **traitement -t** :
![histogramme_option_t.png](Images%20README/histogramme_option_t.png)


## Structure de projet
Notre projet utilise les programmes "gnuplot" et "imagemagick" pour générer des graphiques et les faire pivoter.


## Répartiton du travail
Adam Ben Hamida : Options d1, d2, s, l, le README.md, la réalisation du pdf.

Heeshaam Fowdar : Toute la partie gnuplot.

Thomas Rykaczewski : Option t, les fichiers ".h", le README.md, le Makefile, la réalisation du pdf.


## Auteurs

Nous avons été trois étudiants de PréIng2 MI2 à travailler sur ce projet :
- Adam BEN HAMIDA
- Thomas RYKACZEWSKI
- Heeshaam FOWDAR


## Informations

Professeur d'informatique : Romuald GRIGNON

Classe : PréIng2 MI2 (CY Tech, Cergy)

Date finale de rendu : vendredi 2 février 2024 avant 18h59:59

Lien Gitlab : https://gitlab.com/cy-trucks-pr-ing-2/cy-trucks
